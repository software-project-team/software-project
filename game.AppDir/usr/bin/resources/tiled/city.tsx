<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="city" tilewidth="16" tileheight="16" tilecount="486" columns="27">
 <image source="../graphics/tilesets/city.png" trans="ff00ff" width="432" height="288"/>
 <wangsets>
  <wangset name="ground" type="corner" tile="-1">
   <wangcolor name="grass" color="#33bdae" tile="-1" probability="1"/>
   <wangcolor name="building1" color="#7a77a4" tile="-1" probability="1"/>
   <wangcolor name="building2" color="#b39b7d" tile="-1" probability="1"/>
   <wangcolor name="water" color="#59b6d8" tile="-1" probability="1"/>
   <wangcolor name="concrete" color="#aaa8bd" tile="-1" probability="1"/>
   <wangtile tileid="0" wangid="0,0,0,1,0,0,0,0"/>
   <wangtile tileid="1" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="2" wangid="0,0,0,0,0,1,0,0"/>
   <wangtile tileid="5" wangid="0,1,0,0,0,1,0,1"/>
   <wangtile tileid="6" wangid="0,1,0,1,0,0,0,1"/>
   <wangtile tileid="8" wangid="0,0,0,5,0,0,0,0"/>
   <wangtile tileid="9" wangid="0,0,0,5,0,5,0,0"/>
   <wangtile tileid="10" wangid="0,0,0,0,0,5,0,0"/>
   <wangtile tileid="13" wangid="0,5,0,0,0,5,0,5"/>
   <wangtile tileid="14" wangid="0,5,0,5,0,0,0,5"/>
   <wangtile tileid="27" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="28" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="29" wangid="0,0,0,0,0,1,0,1"/>
   <wangtile tileid="32" wangid="0,0,0,1,0,1,0,1"/>
   <wangtile tileid="33" wangid="0,1,0,1,0,1,0,0"/>
   <wangtile tileid="35" wangid="0,5,0,5,0,0,0,0"/>
   <wangtile tileid="36" wangid="0,5,0,5,0,5,0,5"/>
   <wangtile tileid="37" wangid="0,0,0,0,0,5,0,5"/>
   <wangtile tileid="40" wangid="0,0,0,5,0,5,0,5"/>
   <wangtile tileid="41" wangid="0,5,0,5,0,5,0,0"/>
   <wangtile tileid="54" wangid="0,1,0,0,0,0,0,0"/>
   <wangtile tileid="55" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="56" wangid="0,0,0,0,0,0,0,1"/>
   <wangtile tileid="62" wangid="0,5,0,0,0,0,0,0"/>
   <wangtile tileid="63" wangid="0,5,0,0,0,0,0,5"/>
   <wangtile tileid="64" wangid="0,0,0,0,0,0,0,5"/>
   <wangtile tileid="81" wangid="0,0,0,2,0,0,0,0"/>
   <wangtile tileid="82" wangid="0,0,0,2,0,2,0,0"/>
   <wangtile tileid="83" wangid="0,0,0,0,0,2,0,0"/>
   <wangtile tileid="86" wangid="0,2,0,0,0,2,0,2"/>
   <wangtile tileid="87" wangid="0,2,0,2,0,0,0,2"/>
   <wangtile tileid="89" wangid="0,0,0,3,0,0,0,0"/>
   <wangtile tileid="90" wangid="0,0,0,3,0,3,0,0"/>
   <wangtile tileid="91" wangid="0,0,0,0,0,3,0,0"/>
   <wangtile tileid="94" wangid="0,3,0,0,0,3,0,3"/>
   <wangtile tileid="95" wangid="0,3,0,3,0,0,0,3"/>
   <wangtile tileid="108" wangid="0,2,0,2,0,0,0,0"/>
   <wangtile tileid="109" wangid="0,2,0,2,0,2,0,2"/>
   <wangtile tileid="110" wangid="0,0,0,0,0,2,0,2"/>
   <wangtile tileid="113" wangid="0,0,0,2,0,2,0,2"/>
   <wangtile tileid="114" wangid="0,2,0,2,0,2,0,0"/>
   <wangtile tileid="116" wangid="0,3,0,3,0,0,0,0"/>
   <wangtile tileid="117" wangid="0,3,0,3,0,3,0,3"/>
   <wangtile tileid="118" wangid="0,0,0,0,0,3,0,3"/>
   <wangtile tileid="121" wangid="0,0,0,3,0,3,0,3"/>
   <wangtile tileid="122" wangid="0,3,0,3,0,3,0,0"/>
   <wangtile tileid="135" wangid="0,2,0,0,0,0,0,0"/>
   <wangtile tileid="136" wangid="0,2,0,0,0,0,0,2"/>
   <wangtile tileid="137" wangid="0,0,0,0,0,0,0,2"/>
   <wangtile tileid="143" wangid="0,3,0,0,0,0,0,0"/>
   <wangtile tileid="144" wangid="0,3,0,0,0,0,0,3"/>
   <wangtile tileid="145" wangid="0,0,0,0,0,0,0,3"/>
   <wangtile tileid="170" wangid="0,0,0,4,0,0,0,0"/>
   <wangtile tileid="171" wangid="0,0,0,4,0,4,0,0"/>
   <wangtile tileid="172" wangid="0,0,0,0,0,4,0,0"/>
   <wangtile tileid="175" wangid="0,4,0,0,0,4,0,4"/>
   <wangtile tileid="176" wangid="0,4,0,4,0,0,0,4"/>
   <wangtile tileid="197" wangid="0,4,0,4,0,0,0,0"/>
   <wangtile tileid="198" wangid="0,4,0,4,0,4,0,4"/>
   <wangtile tileid="199" wangid="0,0,0,0,0,4,0,4"/>
   <wangtile tileid="202" wangid="0,0,0,4,0,4,0,4"/>
   <wangtile tileid="203" wangid="0,4,0,4,0,4,0,0"/>
   <wangtile tileid="224" wangid="0,4,0,0,0,0,0,0"/>
   <wangtile tileid="225" wangid="0,4,0,0,0,0,0,4"/>
   <wangtile tileid="226" wangid="0,0,0,0,0,0,0,4"/>
  </wangset>
 </wangsets>
</tileset>
