Build Instruction:

```
git clone https://gitlab.com/software-project-team/software-project.git
cd software-project
make
./software_project
```

Note: for Windows MinGW is needed. Also run `mingw32-make.exe`  instead of `make` and run `./software_project.exe`
