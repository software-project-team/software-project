/*
 * tlos_enitity_importer.cpp
 *
 *  Created on: Dec 24, 2021
 *      Author: ben
 */

#include "tlos_entity_importer.hpp"

namespace tlos {

void entity_importer::import(const nlohmann::json& json, entt::registry& registry, b2World* world, float scale)
{
//	float s = 8.0f;
	for (unsigned int i = 0; i < json["layers"].size(); ++i)
	{
		if (json["layers"][i]["name"] == "entity")
		{
			for (unsigned int j = 0; j < json["layers"][i]["objects"].size(); ++j)
			{
				float x = json["layers"][i]["objects"][j]["x"];
				float y = json["layers"][i]["objects"][j]["y"];

				if (json["layers"][i]["objects"][j]["name"] == "sop")
				{
					tlos::sop::create(registry, world, x*scale, y*scale);
				}
				else if (json["layers"][i]["objects"][j]["name"] == "bat")
				{
					tlos::bat::create(registry, world, x*scale, y*scale);
				}
				else if (json["layers"][i]["objects"][j]["name"] == "checkpoint")
				{
					float w = json["layers"][i]["objects"][j]["width"];
					float h = json["layers"][i]["objects"][j]["height"];
					std::string tmp = json["layers"][i]["objects"][j]["type"];
					int order = std::stoi(tmp);
					tlos::checkpoint::create(registry, x*scale, y*scale, w*scale, h*scale, order);
				}
				else if (json["layers"][i]["objects"][j]["name"] == "sprinter")
				{
					tlos::sprinter::create(registry, world, x*scale, y*scale);
				}
				else if (json["layers"][i]["objects"][j]["name"] == "boss")
				{
					tlos::boss::create(registry, world, x*scale, y*scale);
				}
			}
		}
	}
}

}


