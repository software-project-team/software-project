/*
 * tlos_draw.cpp
 *
 *  Created on: Dec 30, 2021
 *      Author: christian
 */

#include "tlos_draw.hpp"

namespace tlos {

	void draw::debug(entt::registry& registry)
	{
		entt::basic_view view = registry.view<
					spgu::draw_quad,
					tlos::hitbox,
					tlos::hurtbox>();

		for (entt::entity e : view)
		{
			spgu::draw_quad& draw_quad = view.get<spgu::draw_quad>(e);
			tlos::hitbox& hitbox = view.get<tlos::hitbox>(e);
			tlos::hurtbox& hurtbox = view.get<tlos::hurtbox>(e);

			DrawRectangleLinesEx(hitbox.m_hitbox, 1.0f, RED);
			DrawRectangleLinesEx(hurtbox.m_hurtbox, 1.0f, GREEN);
		}
	}

}



