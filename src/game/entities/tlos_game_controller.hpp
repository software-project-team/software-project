/*
 * tlos_game_controller.hpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao
 */

#ifndef SRC_GAME_ENTITIES_TLOS_GAME_CONTROLLER_HPP_
#define SRC_GAME_ENTITIES_TLOS_GAME_CONTROLLER_HPP_

#include "entt.hpp"

#include "spgu_position.hpp"
#include "spgu_draw_quad.hpp"
#include "tlos_bonus_type.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_timer.hpp"
#include "tlos_bonus_item_type.hpp"

namespace tlos {
/**
 * @brief A struct for defining game controller
 * @author gao
 *
 * This is a simple struct for defining game controller.
 */
struct game_controller {
	static void create(entt::registry& registry, float x, float y);
};

}



#endif /* SRC_GAME_ENTITIES_TLOS_GAME_CONTROLLER_HPP_ */
