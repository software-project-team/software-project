/*
 * tloss_boss.hpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao, angelov
 */

#ifndef SRC_GAME_ENTITIES_TLOS_BOSS_HPP_
#define SRC_GAME_ENTITIES_TLOS_BOSS_HPP_



//
#include "entt.hpp"
//#include "box2d/box2d.h"
// spgu


#include "tlos_entity_type.hpp"
#include "tlos_entity_state.hpp"
#include "spgu_capsule.hpp"
#include "spgu_circle.hpp"
#include "spgu_shape.hpp"
#include "spgu_direction.hpp"
#include "tlos_hitbox.hpp"
#include "tlos_hurtbox.hpp"
#include "tlos_speed.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_animation.hpp"
#include "spgu_animation_container.hpp"
#include "spgu_draw_quad.hpp"
#include "tlos_character_stats.hpp"
#include "tlos_boss_type.hpp"
#include "tlos_sight.hpp"




namespace tlos {
/**
 * @brief A class for defining boss
 * @author gao, angelov
 *
 * This is a simple class for defining boss.
 */
class boss {
public:
	static void create(entt::registry& registry, b2World* world, float x, float y);
};

}


#endif /* SRC_GAME_ENTITIES_TLOSS_BOSS_HPP_ */
