/*
 * tlos_diamond.cpp
 *
 *  Created on: Jan 1, 2022
 *      Author: angel
 */

#include "tlos_diamond.hpp"

namespace tlos {

void diamond::create(entt::registry& registry, float x, float y)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::bonus_type>(ent, 10);
	registry.emplace<spgu::position>(ent, x, y);
	registry.emplace<spgu::draw_quad>(ent, 96.0f, 96.0f, 0.0f, 0.0f);
	registry.emplace<spgu::active_animation>(ent, "diamond");
	registry.emplace<spgu::timer>(ent, 3.0f);
	registry.emplace<tlos::bonus_item_type>(ent, tlos::item_type::diamond);
}

}

