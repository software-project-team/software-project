/*
 * tlos_sop.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim, christian
 */

#ifndef SRC_GAME_ENTITIES_TLOS_SOP_HPP_
#define SRC_GAME_ENTITIES_TLOS_SOP_HPP_


//
#include "entt.hpp"
//#include "box2d/box2d.h"
// spgu


#include "tlos_entity_type.hpp"
#include "tlos_entity_state.hpp"
#include "spgu_capsule.hpp"
#include "spgu_circle.hpp"
#include "spgu_shape.hpp"
#include "spgu_direction.hpp"
#include "tlos_hitbox.hpp"
#include "tlos_hurtbox.hpp"
#include "tlos_speed.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_animation.hpp"
#include "spgu_animation_container.hpp"
#include "spgu_draw_quad.hpp"
#include "tlos_character_stats.hpp"
#include "tlos_player_type.hpp"




namespace tlos {
/**
 * @brief A class for defining player
 * @author nasim, christian
 *
 * This is a simple class for defining player.
 */
class sop {
public:
	static void create(entt::registry& registry, b2World* world, float x, float y);
};

}



#endif /* SRC_GAME_ENTITIES_TLOS_SOP_HPP_ */
