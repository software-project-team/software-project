/*
 * tlos_diamond.hpp
 *
 *  Created on: Jan 1, 2022
 *      Author: angel
 */

#ifndef SRC_GAME_ENTITIES_TLOS_DIAMOND_HPP_
#define SRC_GAME_ENTITIES_TLOS_DIAMOND_HPP_

#include "entt.hpp"

#include "spgu_position.hpp"
#include "spgu_draw_quad.hpp"
#include "tlos_bonus_type.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_timer.hpp"
#include "tlos_bonus_item_type.hpp"

namespace tlos {
/**
 * @brief A struct for defining diamond
 * @author angel
 *
 * This is a simple struct for defining diamond.
 */
struct diamond {
	static void create(entt::registry& registry, float x, float y);
};

}


#endif /* SRC_GAME_ENTITIES_TLOS_DIAMOND_HPP_ */
