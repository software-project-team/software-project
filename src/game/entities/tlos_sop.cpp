/*
 * tlos_sop.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim, christian
 */


#include "tlos_sop.hpp"

namespace tlos {

void sop::create(entt::registry& registry, b2World* world, float x, float y)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::entity_type>(ent, tlos::type::player);
	registry.emplace<tlos::entity_state>(ent, tlos::state::idle);
	registry.emplace<spgu::shape*>(ent, new spgu::circle(x, y, 75.0f, b2_dynamicBody, world));
	registry.emplace<tlos::hitbox>(ent, 75.0f, 75.0f, 50.0f, 75.0f);
	registry.emplace<tlos::hurtbox>(ent, 100.0f, 150.0f, 0.0f, 32.0f);
	registry.emplace<spgu::direction>(ent, 0.0f, -1.0f);
	registry.emplace<tlos::speed>(ent, 300.0f);
	registry.emplace<tlos::character_stats>(ent, 5, 1000, 250);
	registry.emplace<spgu::draw_quad>(ent, 96.0f, 96.0f, 0.0f, -50.0f);
	registry.emplace<spgu::active_animation>(ent, "sop_idle");
	spgu::animation_container& animation_container = registry.emplace<spgu::animation_container>(ent);
	animation_container.m_animations.emplace("sop_idle", new spgu::animation(8, 4, 96, 1.0f, true));
	animation_container.m_animations.emplace("sop_walk", new spgu::animation(8, 6, 96, 0.75f, true));
	animation_container.m_animations.emplace("sop_attack", new spgu::animation(8, 3, 96, 0.3f, false));
	animation_container.m_animations.emplace("sop_roll", new spgu::animation(8, 7, 96, 0.7f, false));
	registry.emplace<tlos::player_type>(ent);
	// TODO where am I
}

}
