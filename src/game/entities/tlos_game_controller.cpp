/*
 * tlos_game_controller.cpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao
 */

#include "tlos_game_controller.hpp"

namespace tlos {

void game_controller::create(entt::registry& registry, float x, float y)
{
	entt::entity ent = registry.create();
	registry.emplace<tlos::bonus_type>(ent, 10);
	registry.emplace<spgu::position>(ent, x, y);
	registry.emplace<spgu::draw_quad>(ent, 96.0f, 96.0f, 0.0f, 0.0f);
	registry.emplace<spgu::active_animation>(ent, "controller");
	registry.emplace<spgu::timer>(ent, 3.0f);
	registry.emplace<tlos::bonus_item_type>(ent, tlos::item_type::controller);
}

}


