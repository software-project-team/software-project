/*
 * tlos_bat.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angelov
 */

#ifndef SRC_GAME_ENTITIES_TLOS_BAT_HPP_
#define SRC_GAME_ENTITIES_TLOS_BAT_HPP_



#include "entt.hpp"
#include "box2d/box2d.h"
#include "raylib.h"

// spgu
#include "spgu_shape.hpp"
#include "spgu_circle.hpp"
#include "spgu_rectangle.hpp"
#include "spgu_capsule.hpp"
#include "spgu_direction.hpp"
#include "tlos_entity_state.hpp"
#include "tlos_entity_type.hpp"
#include "spgu_draw_quad.hpp"
#include "spgu_animation.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_animation_container.hpp"
#include "tlos_hitbox.hpp"
#include "tlos_hurtbox.hpp"
#include "tlos_character_stats.hpp"
#include "tlos_bat_type.hpp"
#include "tlos_bonus.hpp"
#include "tlos_speed.hpp"
#include "tlos_sight.hpp"

namespace tlos {
/**
 * @brief A class for defining bat
 * @author angelov
 *
 * This is a simple class for defining bat.
 */
class bat {
public:
	static void create(entt::registry& registry, b2World* world, float x, float y);
};

}


#endif /* SRC_GAME_ENTITIES_TLOS_BAT_HPP_ */
