/*
 * tlos_boss_state.hpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao, angelov, angel
 */

#ifndef SRC_GAME_SYSTEMS_TLOS_BOSS_STATE_HPP_
#define SRC_GAME_SYSTEMS_TLOS_BOSS_STATE_HPP_

#include "raylib.h"
#include "entt.hpp"
#include "tlos_entity_state.hpp"
#include "tlos_entity_type.hpp"
#include "tlos_hurtbox.hpp"
#include "tlos_hitbox.hpp"
#include "spgu_direction.hpp"
#include "tlos_boss_type.hpp"
#include "spgu_active_animation.hpp"
#include "tlos_entity_state.hpp"

namespace tlos {
/**
 * @brief A class which determines the state of the boss
 * @author gao, angelov, angel
 *
 * This is a class which determines the state of the boss.
 */
struct boss_state {
	boss_state();
	void update(entt::registry& registry);

private:
private:
	void idle(tlos::entity_state& state, Rectangle sop_hurtb, Rectangle boss_hitb);
	void walk(tlos::entity_state& state, Rectangle sop_hurtb, Rectangle boss_hitb);
	void attack(tlos::entity_state& state, Rectangle sop_hurtb, Rectangle boss_hitb);

private:
	float m_time;
	float m_attack_cooldown;
};

}  // namespace tlos


#endif /* SRC_GAME_SYSTEMS_TLOS_BOSS_STATE_HPP_ */
