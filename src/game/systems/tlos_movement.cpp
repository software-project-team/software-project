/*
 * tlos_movement.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim, christian
 */

#include "tlos_movement.hpp"

namespace tlos {

void movement::move(entt::registry& registry)
{
	entt::basic_view view =
			registry.view<	tlos::entity_state,
			tlos::entity_type,
			spgu::shape*,
			spgu::draw_quad,
			spgu::direction,
			tlos::hitbox,
			tlos::hurtbox,
			tlos::speed>();

	for (entt::entity e : view)
	{
		tlos::entity_type& type = view.get<tlos::entity_type>(e);
		if (type.m_type == tlos::type::player)
		{
			tlos::entity_state& state = view.get<tlos::entity_state>(e);
			spgu::direction& direction = view.get<spgu::direction>(e);
			spgu::shape*& collider = view.get<spgu::shape*>(e);
			spgu::draw_quad& draw_quad = view.get<spgu::draw_quad>(e);
			tlos::hitbox& hb = view.get<tlos::hitbox>(e);
			tlos::hurtbox& hrb = view.get<tlos::hurtbox>(e);
			tlos::speed& spd = view.get<tlos::speed>(e);
			sop_pos = collider->m_body->GetPosition();
			bool moving = false;
			if (IsKeyDown(KEY_D) || IsKeyDown(KEY_A) || IsKeyDown(KEY_W) || IsKeyDown(KEY_S)
					|| IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_UP) || IsKeyDown(KEY_DOWN))
			{
				direction.m_direction = b2Vec2_zero;
				moving = true;
			}
			if (IsKeyDown(KEY_D) || IsKeyDown(KEY_RIGHT)) direction.m_direction.x = 1;
			if (IsKeyDown(KEY_A) || IsKeyDown(KEY_LEFT)) direction.m_direction.x = -1;
			if (IsKeyDown(KEY_W) || IsKeyDown(KEY_UP)) direction.m_direction.y = 1;
			if (IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN)) direction.m_direction.y = -1;
			if (state.m_state == tlos::state::roll)
			{
				spd.m_speed = 500.0f;
			}
			else
			{
				spd.m_speed = 250.0f;
			}
			if (moving && (state.m_state == tlos::state::walk || state.m_state == tlos::state::roll))
			{
				if (direction.m_direction.x != 0 && direction.m_direction.y != 0)
				{
					float length = direction.m_direction.Length();
					direction.m_direction.x /= length;
					direction.m_direction.y /= length;
				}
				collider->m_body->SetLinearVelocity(
						b2Vec2(spd.m_speed*direction.m_direction.x, spd.m_speed*direction.m_direction.y));
			}
			else
			{
				collider->m_body->SetLinearVelocity(b2Vec2_zero);
			}
			draw_quad.m_x = (collider->m_body->GetPosition().x);
			draw_quad.m_y = (-collider->m_body->GetPosition().y);
			hrb.m_hurtbox.x = hrb.m_offset.x + (collider->m_body->GetPosition().x - hrb.m_hurtbox.width/2.0f);
			hrb.m_hurtbox.y = -hrb.m_offset.y + (-collider->m_body->GetPosition().y - hrb.m_hurtbox.height/2.0f);
			hb.m_hitbox.x = direction.m_direction.x*hb.m_offset.x + (collider->m_body->GetPosition().x - hb.m_hitbox.width/2.0f);
			hb.m_hitbox.y = -direction.m_direction.y*hb.m_offset.y + (-collider->m_body->GetPosition().y - hb.m_hitbox.height/2.0f);
		}
	}

}




void movement::follow_player(entt::registry& registry)
{


	entt::basic_view view = registry.view<
			tlos::entity_type,
			spgu::direction,
			spgu::shape*,
			spgu::draw_quad,
			tlos::hitbox,
			tlos::hurtbox,
			tlos::speed,
			tlos::sight>();

	for (entt::entity e : view)
	{
		tlos::entity_type& type = view.get<tlos::entity_type>(e);
		if (type.m_type == tlos::type::enemy || type.m_type == tlos::type::boss)
		{
			spgu::direction& direction = view.get<spgu::direction>(e);
			spgu::shape*& collider = view.get<spgu::shape*>(e);
			spgu::draw_quad& draw_quad = view.get<spgu::draw_quad>(e);
			tlos::hitbox& hitbox = view.get<tlos::hitbox>(e);
			tlos::hurtbox& hurtbox = view.get<tlos::hurtbox>(e);
			tlos::speed& speed = view.get<tlos::speed>(e);
			tlos::sight& sight = view.get<tlos::sight>(e);
			bool moving = false;
			if (b2Distance(sop_pos, collider->m_body->GetPosition()) < sight.m_sight &&
					b2Distance(sop_pos, collider->m_body->GetPosition()) > 90.0f)
			{
				if (collider->m_body->GetPosition().x + 75.0f/2 < sop_pos.x + 75.0f/2 - 20) {direction.m_direction.x = 1;}
				else if (collider->m_body->GetPosition().x + 75.0f/2 > sop_pos.x + 75.0f/2 + 20) {direction.m_direction.x = -1;}
				else {direction.m_direction.x = 0;} // TODO else if
				moving = true;

			}
			moving = false;
			if (b2Distance(sop_pos, collider->m_body->GetPosition()) < sight.m_sight &&
					b2Distance(sop_pos, collider->m_body->GetPosition()) > 90.0f)
			{

				if (collider->m_body->GetPosition().y + 75.0f/2 < sop_pos.y + 75.0f/2 - 20) {direction.m_direction.y = 1;}
				else if (collider->m_body->GetPosition().y + 75.0f/2 > sop_pos.y + 75.0f/2 + 20) {direction.m_direction.y = -1;}
				else {direction.m_direction.y = 0;}
				moving = true;

			}
			if (moving)
			{
				collider->m_body->SetLinearVelocity(
						b2Vec2(speed.m_speed*direction.m_direction.x, speed.m_speed*direction.m_direction.y));
			}
			else
			{
				collider->m_body->SetLinearVelocity(b2Vec2_zero);
			}
			draw_quad.m_x = (collider->m_body->GetPosition().x);
			draw_quad.m_y = (-collider->m_body->GetPosition().y);
			hurtbox.m_hurtbox.x = hurtbox.m_offset.x + (collider->m_body->GetPosition().x - hurtbox.m_hurtbox.width/2.0f);
			hurtbox.m_hurtbox.y = -hurtbox.m_offset.y + (-collider->m_body->GetPosition().y - hurtbox.m_hurtbox.height/2.0f);
			hitbox.m_hitbox.x = direction.m_direction.x*hitbox.m_offset.x + (collider->m_body->GetPosition().x - hitbox.m_hitbox.width/2.0f);
			hitbox.m_hitbox.y = -direction.m_direction.y*hitbox.m_offset.y + (-collider->m_body->GetPosition().y - hitbox.m_hitbox.height/2.0f);
		}
	}
}

}




