/*
 * tlos_melee.cpp
 *
 *  Created on: Dec 23, 2021
 *      Author: gao, angelov, angel
 */

#include "tlos_melee.hpp"

namespace tlos {

void melee::attack(entt::registry& registry)
{
	entt::basic_view sop = registry.view<tlos::player_type, tlos::hitbox, tlos::hurtbox, spgu::direction, tlos::character_stats>();//,

	Rectangle sop_hitbox;
	Rectangle sop_hurtbox;
	b2Vec2 sop_dir;
	tlos::character_stats* sop_stats;
	for (entt::entity e : sop)
	{
		//tlos::player_type& player_t = sop.get<tlos::player_type>(e);
		tlos::hitbox& hitb = sop.get<tlos::hitbox>(e);
		tlos::hurtbox& hurtb = sop.get<tlos::hurtbox>(e);
		spgu::direction& dir = sop.get<spgu::direction>(e);
		tlos::character_stats& stats = sop.get<tlos::character_stats>(e);
		sop_hitbox = hitb.m_hitbox;
		sop_hurtbox = hurtb.m_hurtbox;
		sop_dir = dir.m_direction;
		sop_stats = &stats;
	}

	// bat
	entt::basic_view bats = registry.view<spgu::shape*, tlos::bat_type, tlos::hurtbox, tlos::character_stats>();//,

	Rectangle bats_hurtbox;
	for (entt::entity e : bats)
	{
	//tlos::bat_type& bat_t = bats.get<tlos::bat_type>(e);
		tlos::hurtbox& hurtb = bats.get<tlos::hurtbox>(e);
		spgu::shape*& collider = bats.get<spgu::shape*>(e);
		tlos::character_stats& stats = bats.get<tlos::character_stats>(e);
		bats_hurtbox = hurtb.m_hurtbox;

		if (CheckCollisionRecs(sop_hitbox, bats_hurtbox) && IsKeyPressed(KEY_X))
		{
			//			collider->m_body->ApplyLinearImpulseToCenter(b2Vec2(sop_dir.x*3000000.0f, sop_dir.y*3000000.0f), true);
			collider->m_body->SetLinearVelocity(b2Vec2(sop_dir.x*3000.0f, sop_dir.y*3000.0f));

			--stats.m_lives;
		}
		if (stats.m_lives <= 0)
		{

			tlos::diamond::create(registry, collider->m_body->GetPosition().x, -collider->m_body->GetPosition().y);
			delete collider;
			registry.destroy(e);
		}
	}
	// sprinter
	entt::basic_view sprinter = registry.view<spgu::shape*, tlos::sprinter_type, tlos::hurtbox, tlos::character_stats>();//,

		Rectangle sprinter_hurtbox;
		for (entt::entity e : sprinter)
		{
		//tlos::bat_type& bat_t = bats.get<tlos::bat_type>(e);
			tlos::hurtbox& hurtb = sprinter.get<tlos::hurtbox>(e);
			spgu::shape*& collider = sprinter.get<spgu::shape*>(e);
			tlos::character_stats& stats = sprinter.get<tlos::character_stats>(e);
			sprinter_hurtbox = hurtb.m_hurtbox;

			if (CheckCollisionRecs(sop_hitbox, sprinter_hurtbox) && IsKeyPressed(KEY_X))
			{
				//			collider->m_body->ApplyLinearImpulseToCenter(b2Vec2(sop_dir.x*3000000.0f, sop_dir.y*3000000.0f), true);
				collider->m_body->SetLinearVelocity(b2Vec2(sop_dir.x*3000.0f, sop_dir.y*3000.0f));

				--stats.m_lives;

			}
			if (stats.m_lives <= 0)
			{

				tlos::diamond::create(registry, collider->m_body->GetPosition().x, -collider->m_body->GetPosition().y);
				delete collider;
				registry.destroy(e);
			}
		}

		// boss
		entt::basic_view boss = registry.view<spgu::shape*, tlos::boss_type, tlos::hurtbox, tlos::character_stats>();//,

		Rectangle boss_hurtbox;
		for (entt::entity e : boss)
		{
			//tlos::bat_type& bat_t = bats.get<tlos::bat_type>(e);
			tlos::hurtbox& hurtb = boss.get<tlos::hurtbox>(e);
			spgu::shape*& collider = boss.get<spgu::shape*>(e);
			tlos::character_stats& stats = boss.get<tlos::character_stats>(e);
			boss_hurtbox = hurtb.m_hurtbox;

			if (CheckCollisionRecs(sop_hitbox, boss_hurtbox) && IsKeyPressed(KEY_X))
			{
				//			collider->m_body->ApplyLinearImpulseToCenter(b2Vec2(sop_dir.x*3000000.0f, sop_dir.y*3000000.0f), true);
				collider->m_body->SetLinearVelocity(b2Vec2(sop_dir.x*3000.0f, sop_dir.y*3000.0f));

				--stats.m_lives;

			}
			if (stats.m_lives <= 0)
			{

				tlos::game_controller::create(registry, collider->m_body->GetPosition().x, -collider->m_body->GetPosition().y);
				delete collider;
				registry.destroy(e);
			}
		}
}

void melee::take_damage(entt::registry& registry)
{
	entt::basic_view sop = registry.view<tlos::player_type, spgu::shape*, tlos::character_stats, tlos::hurtbox>();

	Rectangle sop_hurtbox;
	spgu::shape* sop_collider;
	tlos::character_stats* sop_stats;
	for (entt::entity e : sop)
	{
		tlos::hurtbox& s_hrb = sop.get<tlos::hurtbox>(e);
		spgu::shape*& collider = sop.get<spgu::shape*>(e);
		tlos::character_stats& stats = sop.get<tlos::character_stats>(e);
		sop_hurtbox = s_hrb.m_hurtbox;
		sop_collider = collider;
		sop_stats = &stats;
	}

	// bat
	entt::basic_view bats = registry.view<tlos::bat_type, tlos::hitbox>();//,

	Rectangle bats_hitbox;
	for (entt::entity e : bats)
	{
		tlos::hitbox& hitb = bats.get<tlos::hitbox>(e);
		bats_hitbox = hitb.m_hitbox;

		if (CheckCollisionRecs(sop_hurtbox, bats_hitbox) && !IsKeyDown(KEY_D) && !IsKeyPressed(KEY_X))
		{
			sop_stats->m_health -= 1;
			if (sop_stats->m_health <= 0)
			{
				--sop_stats->m_lives;
				sop_stats->m_health = sop_stats->m_max_health;
			}
			//			sop_collider->m_body->ApplyLinearImpulseToCenter(b2Vec2(3000000.0f, 3000000.0f), true);
			//			sop_collider->m_body->SetLinearVelocity(b2Vec2(3000.0f, 3000.0f));
		}

	}

	// sprinter
	entt::basic_view sprinter = registry.view<tlos::sprinter_type, tlos::hitbox>();//,

		Rectangle sprinter_hitbox;
		for (entt::entity e : sprinter)
		{
			tlos::hitbox& hitb = sprinter.get<tlos::hitbox>(e);
			sprinter_hitbox = hitb.m_hitbox;

			if (CheckCollisionRecs(sop_hurtbox, sprinter_hitbox) && !IsKeyDown(KEY_D) && !IsKeyPressed(KEY_X))
			{
				sop_stats->m_health -= 1;
				if (sop_stats->m_health <= 0)
				{
					--sop_stats->m_lives;
					sop_stats->m_health = sop_stats->m_max_health;
				}
				//			sop_collider->m_body->ApplyLinearImpulseToCenter(b2Vec2(3000000.0f, 3000000.0f), true);
				//			sop_collider->m_body->SetLinearVelocity(b2Vec2(3000.0f, 3000.0f));
			}

		}

		// boss
		entt::basic_view boss = registry.view<tlos::boss_type, tlos::hitbox>();//,

		Rectangle boss_hitbox;
		for (entt::entity e : boss)
		{
			tlos::hitbox& hitb = boss.get<tlos::hitbox>(e);
			boss_hitbox = hitb.m_hitbox;

			if (CheckCollisionRecs(sop_hurtbox, boss_hitbox) && !IsKeyDown(KEY_D) && !IsKeyPressed(KEY_X))
			{
				sop_stats->m_health -= 2;
				if (sop_stats->m_health <= 0)
				{
					--sop_stats->m_lives;
					sop_stats->m_health = sop_stats->m_max_health;
				}
				//			sop_collider->m_body->ApplyLinearImpulseToCenter(b2Vec2(3000000.0f, 3000000.0f), true);
				//			sop_collider->m_body->SetLinearVelocity(b2Vec2(3000.0f, 3000.0f));
			}

		}

}
//
}

