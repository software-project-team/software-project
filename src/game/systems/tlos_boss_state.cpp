/*
 * tlos_boss_state.cpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao, angelov, angel
 */

#include "tlos_boss_state.hpp"

namespace tlos {


boss_state::boss_state() : m_time(0.0f), m_attack_cooldown(0.4)
		{}

void boss_state::update(entt::registry& registry)
{
	entt::basic_view sop = registry.view<tlos::entity_state, tlos::entity_type, tlos::hurtbox>();
	tlos::hurtbox* sop_hurtbox;
	for (entt::entity e : sop)
	{
		tlos::entity_type& type = sop.get<tlos::entity_type>(e);
		if (type.m_type == tlos::type::player)
		{
			tlos::hurtbox& hb = sop.get<tlos::hurtbox>(e);
			sop_hurtbox = &hb;
		}
	}

	entt::basic_view view = registry.view<tlos::entity_state, tlos::entity_type, tlos::hitbox>();

	for (entt::entity e : view)
	{
		tlos::entity_type& type = view.get<tlos::entity_type>(e);
		if (type.m_type == tlos::type::boss)
		{
			tlos::entity_state& state = view.get<tlos::entity_state>(e);
			tlos::hitbox& hit_box = view.get<tlos::hitbox>(e);
			if (state.m_state == tlos::state::idle) idle(state, sop_hurtbox->m_hurtbox, hit_box.m_hitbox);
			else if (state.m_state == tlos::state::walk) walk(state, sop_hurtbox->m_hurtbox, hit_box.m_hitbox);
			else if (state.m_state == tlos::state::attack) attack(state, sop_hurtbox->m_hurtbox, hit_box.m_hitbox);
		}
	}

	entt::basic_view health = registry.view<tlos::boss_type, spgu::active_animation, tlos::entity_state>();
	for (entt::entity e : health)
	{
		spgu::active_animation& active = health.get<spgu::active_animation>(e);
		tlos::entity_state& state = health.get<tlos::entity_state>(e);

		if (state.m_state == tlos::state::idle)
		{
			active.m_name = "wn_idle";
		}
		else if (state.m_state == tlos::state::walk)
		{
			active.m_name = "wn_walk";
		}
		else if (state.m_state == tlos::state::attack)
		{
			active.m_name = "wn_attack";
		}
	}
}

void boss_state::idle(tlos::entity_state& state, Rectangle sop_hurtb, Rectangle boss_hitb)
{
	float dx = sop_hurtb.x - boss_hitb.x;
	float dy = sop_hurtb.y - boss_hitb.y;
	if ((dx <= 300 || dx >= -300 || dy <= 300 || dy >= -300) && !CheckCollisionRecs(sop_hurtb, boss_hitb))
	{
		state.m_state = tlos::state::walk;
	}
	else if (CheckCollisionRecs(sop_hurtb, boss_hitb))
	{
		state.m_state = tlos::state::attack;
	}

}

void boss_state::walk(tlos::entity_state& state, Rectangle sop_hurtb, Rectangle boss_hitb)
{
	float dx = sop_hurtb.x - boss_hitb.x;
	float dy = sop_hurtb.y - boss_hitb.y;
	if (!CheckCollisionRecs(sop_hurtb, boss_hitb) && !(dx <= 300 || dx >= -300 || dy <= 300 || dy >= -300))
	{
		state.m_state = tlos::state::idle;
	}
	else if (CheckCollisionRecs(sop_hurtb, boss_hitb))
	{
		state.m_state = tlos::state::attack;
	}
}

void boss_state::attack(tlos::entity_state& state, Rectangle sop_hurtb, Rectangle boss_hitb)
{
	float dx = sop_hurtb.x - boss_hitb.x;
	float dy = sop_hurtb.y - boss_hitb.y;
	m_time += GetFrameTime();
	if ((m_time >= m_attack_cooldown) && (!CheckCollisionRecs(sop_hurtb, boss_hitb) && !(dx <= 300 || dx >= -300 || dy <= 300 || dy >= -300)))
	{
		state.m_state = tlos::state::idle;
		m_time = 0.0f;
	}
	else if ((m_time >= m_attack_cooldown) && !CheckCollisionRecs(sop_hurtb, boss_hitb) && (dx <= 300 || dx >= -300 || dy <= 300 || dy >= -300))
	{
		state.m_state = tlos::state::walk;
		m_time = 0.0f;
	}
}


//

}


