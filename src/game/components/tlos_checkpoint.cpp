/*
 * tlos_check_point.cpp
 *
 *  Created on: Dec 28, 2021
 *      Author: fatma
 */

#include "tlos_checkpoint.hpp"

namespace tlos {
void checkpoint::create(entt::registry& registry, float x, float y, float width, float height, int order)
{
	entt::entity ent = registry.create();
	registry.emplace<spgu::box>(ent, x, y, width, height);
	registry.emplace<spgu::point_order>(ent,order);

}

}  // namespace tlos

