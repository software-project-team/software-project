/*
 * tlos_bonus.hpp
 *
 *  Created on: Jan 1, 2022
 *      Author: angel
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_BONUS_HPP_
#define SRC_GAME_COMPONENTS_TLOS_BONUS_HPP_


namespace tlos {
/**
 * @brief A class for defining diamond bonus
 * @author angel
 *
 * This is a simple class for defining the diamind bonus.
 */
struct bonus {
	bonus(int value);
	int m_value;
};

}  // namespace tlos


#endif /* SRC_GAME_COMPONENTS_TLOS_BONUS_HPP_ */
