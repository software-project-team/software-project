/*
 * tlos_hurtbox.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angelov
 */

#include "tlos_hurtbox.hpp"

namespace tlos {

hurtbox::hurtbox(float width, float height, float offset_x, float offset_y)
:	m_hurtbox({0.0f, 0.0f, width, height}),  m_offset({offset_x, offset_y})
{}

hurtbox::~hurtbox()
{}

}

