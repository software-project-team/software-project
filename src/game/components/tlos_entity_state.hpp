/*
 * tlos_state.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: gao
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_ENTITIY_STATE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_ENTITIY_STATE_HPP_



namespace tlos {

enum struct state {
	spawn,
	idle,
	walk,
	attack,
	roll,
	death
};
/**
 * @brief A struct for defining states
 * @author gao
 *
 * This is a simple struct for defining state.
 */
struct entity_state {
public:
	entity_state(tlos::state state);

public:
	tlos::state m_state;
};

}



#endif /* SRC_GAME_COMPONENTS_TLOS_STATE_HPP_ */
