/*
 * tlos_sight.hpp
 *
 *  Created on: Jan 2, 2022
 *      Author: gao
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_SIGHT_HPP_
#define SRC_GAME_COMPONENTS_TLOS_SIGHT_HPP_

namespace tlos {
/**
 * @brief A struct for defining sight
 * @author gao
 *
 * This is a simple struct for defining sight
 */
struct sight {
	sight(float sight);

	float m_sight;
};

}  // namespace tlos



#endif /* SRC_GAME_COMPONENTS_TLOS_SIGHT_HPP_ */
