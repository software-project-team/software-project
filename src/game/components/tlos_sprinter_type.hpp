/*
 * tlos_sprinter_type.hpp
 *
 *  Created on: Jan 2, 2022
 *      Author: gao
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_SPRINTER_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_SPRINTER_TYPE_HPP_


namespace tlos {
/**
 * @brief A struct for defining sprinter type
 * @author gao
 *
 * This is a simple struct for defining sprinter type.
 */
struct sprinter_type {
	sprinter_type();

	char m_c;
};

}  // namespace tlos



#endif /* SRC_GAME_COMPONENTS_TLOS_SPRINTER_TYPE_HPP_ */
