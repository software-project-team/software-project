/*
 * tlos_bat_type.hpp
 *
 *  Created on: Dec 23, 2021
 *      Author: angelov
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_BAT_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_BAT_TYPE_HPP_

namespace tlos {
/**
 * @brief A class for defining bat type
 * @author angelov
 *
 * This is a simple class for defining the specific enemey type.
 */

struct bat_type {
	bat_type();

private:
	char m_c;
};

}  // namespace tlos



#endif /* SRC_GAME_COMPONENTS_TLOS_BAT_TYPE_HPP_ */
