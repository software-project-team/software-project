/*
 * tlos_boss_type.hpp
 *
 *  Created on: Jan 4, 2022
 *      Author: gao, angelov, angel
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_BOSS_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_BOSS_TYPE_HPP_

namespace tlos {
/**
 * @brief A class for defining boss type
 * @author gao, angelov, angel
 *
 * This is a simple class for defining the boss type.
 */
struct boss_type {
	boss_type();

private:
	char m_c;
};

}  // namespace tlos




#endif /* SRC_GAME_COMPONENTS_TLOS_BOSS_TYPE_HPP_ */
