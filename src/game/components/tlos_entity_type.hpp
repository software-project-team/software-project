/*
 * tlos_entity_type.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: gao
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_ENTITY_TYPE_HPP_
#define SRC_GAME_COMPONENTS_TLOS_ENTITY_TYPE_HPP_

namespace tlos {

enum struct type {
	player,
	enemy,
	npc,
	item,
	boss
};
/**
 * @brief A struct for defining entitiy type
 * @author gao
 *
 * This is a simple struct for defining entitiy type.
 */
struct entity_type {
	entity_type(tlos::type type);

	tlos::type m_type;
};

}



#endif /* SRC_GAME_COMPONENTS_TLOS_ENTITY_TYPE_HPP_ */
