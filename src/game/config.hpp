/*
 * config.hpp
 *
 *  Created on: Nov 15, 2021
 *      Author: yusuf
 */

#ifndef SRC_GAME_CONFIG_HPP_
#define SRC_GAME_CONFIG_HPP_

#include <string>
/**
 * @brief A file for configuration
 * @author yusuf
 *
 * This is a file for configuration.
 */
static const bool DEBUG = true;
static const char* const GAME_NAME = "The Legend of Sop";
static const int FPS = 60;
static const float TILE_SIZE = 96;
static const int WINDOW_WIDTH = TILE_SIZE * 12;
static const int WINDOW_HEIGHT = TILE_SIZE * 9;
static const std::string STORY = "Once upon a time, in a land far far away there was a young\nboy named Sop who lived in a beautiful house surrounded by a\nforest and monsters. He was very afraid of monsters\nso he  never dared to get close to the forest with any step.\nSop had nothing to do but play computer games all day long,\nhe was even known as a legend of playing games. However,\ngood times didn't last long, when his reputation reached Monsterland.\nMr Waternoose, the king of the Monsterland was very jealous of him\nand stole the game controller from Sop. Although, Sop was\nvery sad and hopeless, he decided to overcome his fears\nand go to get his controller back from Mr. Waternoose\nno matter what... ";
static const std::string HOW_TO_PLAY = "Player should follow the game map\nin order to reach the Mr. Waternoose's\nhouse to take his controller back.\nWhen the player defeats the\nMr. Waternoose, he/she has to pick the\ngame controller to win the game.";



#endif /* SRC_GAME_CONFIG_HPP_ */
