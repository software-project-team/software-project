// Let Catch2 know that we are using its main function.
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

// This is possible because of -I../

//#include "/home/fsk/unit-test-game/software-project-new/src/game/stats.hpp"
#include "stats.hpp"

// For the floating point example.
using namespace Catch::Matchers;

TEST_CASE( "Score should work" ) {
	int result = score(5);
	
	// Test.
	REQUIRE(result == 10);
	
	result = score(4);

	// Test.
	REQUIRE(result == 8);

	result = score(24);

	// Test.
	REQUIRE(result == 48);
}

TEST_CASE( "Max health should work" ) {
	int result = max_health();
	
	// Test.
	REQUIRE(result == 5);

}

TEST_CASE( "Health should work" ) {
	int result = health(5);

	// Test.
	REQUIRE(result == 5);

	result = health(4);

	// Test.
	REQUIRE(result == 4);

	result = health(24);

	// Test.
	REQUIRE(result == 5);

}

TEST_CASE( "Game result should work" ) {
	int result = game_result(2);

	// Test.
	REQUIRE(result == 0);

	result = game_result(0);

	// Test.
	REQUIRE(result == 1);

}

TEST_CASE( "Game states should work" ) {
	int result = game_state("menu");

	// Test.
	REQUIRE(result == 0);

	result = game_state("run");

	// Test.
	REQUIRE(result == 1);

	result = game_state("game_over");

	// Test.
	REQUIRE(result == 2);

	result = game_state("win");

	// Test.
	REQUIRE(result == 3);

}






