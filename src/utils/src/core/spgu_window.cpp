/*
 * spgu_window.cpp
 *
 *  Created on: Dec 22, 2021
 *      Author: ben
 */

#include "spgu_window.hpp"

namespace spgu {

window::window(int width, int height, const char* title)
{
	// window flags
	InitWindow(width, height, title);
    InitAudioDevice();
}

window::window(int width, int height, const char* title, const char* icon_path)
{
	// window flags
	InitWindow(width, height, title);
	Image icon;
	icon = LoadImage(icon_path);
	SetWindowIcon(icon);
	UnloadImage(icon);
}

window::~window()
{
	CloseWindow();
}


}


