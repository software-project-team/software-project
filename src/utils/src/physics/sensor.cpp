/*
 * sensor.cpp
 *
 *  Created on: Nov 22, 2021
 *      Author: ben
 */

#include "sensor.hpp"


spgu::physics::sensor::sensor(spgu::shape* body)
:	m_body(body)
{
	m_body->m_body->SetType(b2_dynamicBody);
	//	m_body->get_body()->Get
	m_body->m_body->GetFixtureList();//->//SetSensor(true);

	for (b2Fixture* f = m_body->m_body->GetFixtureList(); f; f = f->GetNext())
	{
		f->SetSensor(true);
	}
}

b2Body* spgu::physics::sensor::get_body() {return m_body->m_body;}
bool spgu::physics::sensor::is_touching() {return m_body->m_body;}






