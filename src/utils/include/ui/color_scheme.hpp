/*
 * color_scheme.hpp
 *
 *  Created on: Nov 24, 2021
 *      Author: crystal
 */

#ifndef SRC_UTILS_INCLUDE_UI_COLOR_SCHEME_HPP_
#define SRC_UTILS_INCLUDE_UI_COLOR_SCHEME_HPP_

#include "raylib.h"

namespace spgu {
namespace ui {
class color_scheme;
}
}
/**
 * @brief A class for defining color scheme
 * source of the color scheme https://bootstrap-vue.org/docs/reference/color-variants
 * @author ysf
 *
 * This is a simple class for creating color scheme.
 */
class spgu::ui::color_scheme {
public:
	/**
	 * Color for primary color
	 */
	Color primary = {0, 123, 255, 255};
	/**
	 * Color for secondary color
	 */
	Color secondary = {108, 117, 125, 255};
	/**
	 * Color for success color
	 */
	Color success = {40, 167, 69, 255};
	/**
	 * Color for warning color
	 */
	Color warning = {255, 193, 7, 255};
	/**
	 * Color for danger color
	 */
	Color danger = {220, 53, 69, 255};
	/**
	 * Color for info color
	 */
	Color info = {23, 162, 184, 255};
	/**
	 * Color for light color
	 */
	Color light = {248, 249, 250, 255};
	/**
	 * Color for dark color
	 */
	Color dark = {52, 58, 64, 255};

private:
};


#endif /* SRC_UTILS_INCLUDE_UI_COLOR_SCHEME_HPP_ */
