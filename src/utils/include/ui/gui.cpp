/*
 * gui.cpp
 *
 *  Created on: Nov 24, 2021
 *      Author: nasim
 */

#include "gui.hpp"

spgu::ui::gui::gui()
{
	m_color_scheme = spgu::ui::color_scheme();
	m_roundness = 0.5f;
}

bool spgu::ui::gui::button(Rectangle rec, const char* text)
{
	int ts = MeasureText(text, 100);
	if (IsMouseButtonDown(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(GetMousePosition(), rec))
	{
		DrawRectangleRounded(rec, m_roundness, 10, m_color_scheme.secondary);
		DrawText(text, rec.x + rec.width/2 - ts/2, rec.y, 100, m_color_scheme.light);
	}
	else
	{
		DrawRectangleRounded(rec, m_roundness, 10, m_color_scheme.primary);
		DrawText(text, rec.x + rec.width/2 - ts/2, rec.y, 100, m_color_scheme.light);

	}
	return IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(GetMousePosition(), rec);
}

bool spgu::ui::gui::image_button(Rectangle rec, Texture2D image)
{
	bool pressed = false;
	if (CheckCollisionPointRec(GetMousePosition(), rec))
	{
		DrawTexturePro
		(
				image,
				(Rectangle) {0.0f, 0.0f, (float) image.width, (float) image.height},
				rec,
				{0.0f,0.0f},
				0.0f,
				{(unsigned char) 150, (unsigned char) 150, (unsigned char) 150, (unsigned char) 255}
		);
		if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
		{
			pressed = true;
		}
	}
	else
	{
		DrawTexturePro
		(
				image,
				(Rectangle) {0.0f, 0.0f, (float) image.width, (float) image.height},
				rec,
				{0.0f,0.0f},
				0.0f,
				WHITE
		);
	}

	return pressed;
}

bool spgu::ui::gui::checkbox(Texture2D texture, Rectangle rec, bool state)
{
	int width = texture.width;
	int height = texture.height;

	if (state)
	{
		DrawTexturePro
		(
				texture,
				{0.0f, 0.0f, width/2.0f, (float) height},
				rec,
				{0.0f, 0.0f},
				0.0f,
				WHITE
		);
	}
	else
	{
		DrawTexturePro
		(
				texture,
				{width/2.0f, 0.0f, width/2.0f, (float) height},
				rec,
				{0.0f, 0.0f},
				0.0f,
				WHITE
		);
	}
	if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(GetMousePosition(), rec)) {return !state;}
	else {return state;}
}

bool spgu::ui::gui::checkbox(Rectangle rec, const char* text, bool state)
{
	float border = 10.0f;
	DrawRectangleRounded(rec, m_roundness, 10, m_color_scheme.primary);
	DrawRectangleRounded({rec.x + border/2, rec.y + border/2, rec.width - border, rec.height - border},
					m_roundness, 10, m_color_scheme.light);
	if (state)
	{
		border *= 2.0f;
		DrawRectangleRounded(
				{rec.x + border/2, rec.y + border/2, rec.width - border, rec.height - border},
				m_roundness, 10, m_color_scheme.primary);
	}
	if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && CheckCollisionPointRec(GetMousePosition(), rec)) {return !state;}
	else {return state;}
}

void spgu::ui::gui::progress_bar(Texture2D texture, Rectangle rec, float scale, int value, int max)
{

	// progress 				stats.m_health*260/stats.m_max_health
	DrawTextureTiled
	(
			texture,
			{54.0f, 0.0f, 3.0f, 8.0f},
			{rec.x, rec.y, (float) (value*rec.width/max), rec.height},
			{0.0f, 0.0f},
			0.0f,
			scale,
			{
					(unsigned char) (255 - (value*125/max)),
					(unsigned char) (0 + (value*255/max)),
					0,
					255
			}
	);
	// border
	DrawTexturePro(texture, {0.0f, 0.0f, 54.0f, 8.0f}, rec, {0.0f, 0.0f}, 0.0f, WHITE);

}

void spgu::ui::gui::n_times(Texture2D texture, float x, float y, float scale, int n_horizontal, int n_vertical)
{
	if (n_horizontal > 0 && n_vertical > 0)
	{
		float width = texture.width;
		float height = texture.height;
		DrawTextureTiled
		(
				texture,
				{0.0f, 0.0f, width, height},
				{x, y, width*scale*n_horizontal, height*scale*n_vertical},
				{0.0f, 0.0f},
				0.0f,
				scale,
				WHITE
		);
	}
}


