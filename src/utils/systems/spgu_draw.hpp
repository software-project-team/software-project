/*
 * spgu_draw.hpp
 *
 *  Created on: Dec 21, 2021
 *      Author: yusuf
 */

#ifndef SRC_UTILS_SYSTEMS_SPGU_DRAW_HPP_
#define SRC_UTILS_SYSTEMS_SPGU_DRAW_HPP_


#include <iostream>
#include "raylib.h"
#include "entt.hpp"
#include "box2d/box2d.h"

#include "spgu_map.hpp"
#include "spgu_circle.hpp"
#include "spgu_capsule.hpp"
#include "spgu_direction.hpp"
#include "spgu_resources.hpp"
#include "spgu_draw_quad.hpp"
#include "spgu_animation.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_animation_container.hpp"




namespace spgu {
/**
 * @brief A class which draws map and animations.
 * @author yusuf
 *
 * This is a class which draws map and animations.
 */
class draw {
public:
	draw(float map_scale, float animation_scale, int safety_row, int safety_col);
	void map(spgu::resources& res, spgu::map& map, std::string layer_name, b2Vec2 focus);
	void debug(b2World* world);
	void animations(spgu::resources& res, entt::registry& registry);
	// TODO non-looping animations
	float m_map_scale;
	float m_animation_scale;
	int m_safety_row;
	int m_safety_col;

};

}



#endif /* SRC_UTILS_SYSTEMS_SPGU_DRAW_HPP_ */
