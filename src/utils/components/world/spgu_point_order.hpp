/*
 * spgu_point_order.hpp
 *
 *  Created on: Jan 2, 2022
 *      Author: fsk
 */

#ifndef SRC_UTILS_COMPONENTS_WORLD_SPGU_POINT_ORDER_HPP_
#define SRC_UTILS_COMPONENTS_WORLD_SPGU_POINT_ORDER_HPP_

namespace spgu {
/**
 * @brief A class for defining the point order
 * @author ysf
 *
 * This is a simple class for creating the point orders for checkpoints.
 */
class point_order {
public:
	/**
	 * Constructor that creates the point order.
	 *
	 * @param order order of the check point
	 */
	point_order(int order);
	/**
	 * int for the order
	 */
	int m_data;
};

}  // namespace spgu



#endif /* SRC_UTILS_COMPONENTS_WORLD_SPGU_POINT_ORDER_HPP_ */
