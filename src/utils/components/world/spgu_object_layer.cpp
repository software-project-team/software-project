/*
 * spgu_object_layer.cpp
 *
 *  Created on: Dec 21, 2021
 *      Author: yusuf
 */

#include "spgu_object_layer.hpp"

namespace spgu {


object_layer::object_layer(const nlohmann::json& json, int layer_index, b2World* world, float scale)
{
	m_data = std::vector<spgu::shape*>();

//	float s = 8.0f;
	for (unsigned int i = 0; i < json["layers"].size(); ++i)
	{
		if (json["layers"][i]["name"] == "collision")
		{
			for (unsigned int j = 0; j < json["layers"][i]["objects"].size(); ++j)
			{
				float x = json["layers"][i]["objects"][j]["x"];
				float y = json["layers"][i]["objects"][j]["y"];
				float w = json["layers"][i]["objects"][j]["width"];
				float h = json["layers"][i]["objects"][j]["height"];

				if (json["layers"][i]["objects"][j]["type"] == "rectangle")
				{

					m_data.emplace_back(new spgu::rectangle(
							x*scale+w*scale/2,
							y*scale+h*scale/2,
							w*scale,
							h*scale,
							b2_staticBody,
							world
					));
				}
				else if (json["layers"][i]["objects"][j]["type"] == "circle")
				{
					m_data.emplace_back(new spgu::circle(
							x*scale+w*scale/2,
							y*scale+h*scale/2,
							w*scale,
							b2_staticBody,
							world
					));
				}
				else if (json["layers"][i]["objects"][j]["type"] == "capsule")
				{
					m_data.emplace_back(new spgu::capsule(
							x*scale+w*scale/2,
							y*scale+h*scale/2,
							w*scale,
							h*scale,
							true,
							b2_staticBody,
							world
					));
				}
			}
		}
	}
}

object_layer::~object_layer()
{
	for (unsigned int i = 0; i < m_data.size(); ++i)
	{
		delete (m_data[i]);
	}
	m_data.clear();
}

}



