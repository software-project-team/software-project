/*
 * spgu_tile_layer.hpp
 *
 *  Created on: Dec 21, 2021
 *      Author: yusuf
 */

#ifndef SRC_UTILS_COMPONENTS_WORLD_SPGU_TILE_LAYER_HPP_
#define SRC_UTILS_COMPONENTS_WORLD_SPGU_TILE_LAYER_HPP_



#include <iostream>

#include "json.hpp"


namespace spgu {
/**
 * @brief A class for importing tile layer from "Tiled" map
 * @author ysf
 *
 * This is a simple class for importing tile layer from "Tiled" map
 */

class tile_layer {
public:
	/**
	 * Constructor that imports the tile layer from a JSON object.
	 *
	 * @param json Reference to json object
	 * @param layer_index Index of the layer
	 */
	tile_layer(const nlohmann::json& json, int layer_index);
	~tile_layer();

public:
	/**
	 * number of columns in the tile layer
	 */
	unsigned int m_column;
	/**
	 * number of rows in the tile layer
	 */
	unsigned int m_row;
	/**
	 * size of the tiles
	 */
	float m_tile_size;
public:
	/**
	 * 2D int array that stores tile data
	 */
	int** m_data;
};

}


#endif /* SRC_UTILS_COMPONENTS_WORLD_SPGU_TILE_LAYER_HPP_ */
