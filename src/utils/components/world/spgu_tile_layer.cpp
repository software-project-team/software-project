/*
 * spgu_tile_layer.cpp
 *
 *  Created on: Dec 21, 2021
 *      Author: yusuf
 */

#include "spgu_tile_layer.hpp"

namespace spgu {

tile_layer::tile_layer(const nlohmann::json& json, int layer_index)
{
	m_column = json["width"];
	m_row = json["height"];
	m_tile_size = json["tilewidth"];
	m_data = new int*[m_row];

	// 2 dimensional array
	for (unsigned int i = 0; i < m_row; ++i)
	{
		m_data[i] = new int[m_column];
		assert(m_data[i]);
	}
	// take data from json to array
	for (unsigned int r = 0; r < m_row; ++r)
	{
		for (unsigned int c = 0; c < m_column; ++c)
		{
			m_data[r][c] = json["layers"][layer_index]["data"][r*m_column+c];
		}
	}
}

tile_layer::~tile_layer()
{
	for (unsigned int r = 0; r < m_row; r++)
	{
		delete [] m_data[r];
	}
	delete [] m_data;
}

}


