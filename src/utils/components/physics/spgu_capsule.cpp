/*
 * spgu_capsule.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: christian
 */
#include "spgu_capsule.hpp"

namespace spgu {

capsule::capsule(float x, float y, float width, float height, bool horizontal, b2BodyType type, b2World* world)
: spgu::shape(x, y, type, world)
{
	// fixture definition
	b2FixtureDef fixtureDef;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.0f;
	// rectangle
	b2PolygonShape rect;
	rect.SetAsBox(width/2.0f, height/2.0f);
	fixtureDef.shape = &rect;
	m_body->CreateFixture(&fixtureDef);
	// circles
	b2CircleShape circle;
	fixtureDef.shape = &circle;

	if (horizontal)
	{
		circle.m_radius = height/2.0f;
		circle.m_p = b2Vec2(width/2.0f, 0.0f);
		m_body->CreateFixture(&fixtureDef);
		circle.m_p = b2Vec2(-width/2.0f, 0.0f);
		m_body->CreateFixture(&fixtureDef);
	}
	else
	{
		circle.m_radius = width/2.0f;
		circle.m_p = b2Vec2(0.0f, height/2.0f);
		m_body->CreateFixture(&fixtureDef);
		circle.m_p = b2Vec2(0.0f, -height/2.0f);
		m_body->CreateFixture(&fixtureDef);
	}
}

capsule::~capsule() {}


}


