/*
 * spgu_direction.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_DIRECTION_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_DIRECTION_HPP_


#include "box2d/box2d.h"

namespace spgu {
/**
 * @brief A class for defining direction vector
 * @author ysf
 *
 * This is a simple class for defining direction vector
 */

class direction {
public:
	/**
	 * Constructor that creates the direction.
	 *
	 * @param x Value of the vector
	 * @param y Value of the vector
	 */
	direction(float x, float y);

public:
	/**
	 * b2Vec2 vector for the direction
	 */
	b2Vec2 m_direction;
};
}



#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_DIRECTION_HPP_ */
