/*
 * spgu_draw_quad.hpp
 *
 *  Created on: Dec 26, 2021
 *      Author: nasim
 */

#ifndef SRC_UTILS_COMPONENTS_ANIMATION_SPGU_DRAW_QUAD_HPP_
#define SRC_UTILS_COMPONENTS_ANIMATION_SPGU_DRAW_QUAD_HPP_

#include "raylib.h"

namespace spgu {
/**
 * @brief A struct for defining quadrat
 * @author ysf
 *
 * This is a simple struct for drawing texture in a position with a specified size.
 */
struct draw_quad {
	/**
	 * Constructor that crates the quadrat.
	 *
	 * @param width Desired width of texture
	 * @param height Desired height of the shape
	 * @param offset_x Offset in X coordinate
	 * @param offset_y Offset in Y coordinate
	 */
	draw_quad(float width, float height, float offset_x, float offset_y);
	/**
	 * float for the position in X coordinate
	 */
	float m_x;
	/**
	 * float for the position in Y coordinate
	 */
	float m_y;	/**
	 * float for the width
	 */
	float m_width;
	/**
	 * float for the height
	 */
	float m_height;
	/**
	 * float for the offset in X coordinate
	 */
	float m_offset_x;
	/**
	 * float for the offset in Y coordinate
	 */
	float m_offset_y;
};

}



#endif /* SRC_UTILS_COMPONENTS_ANIMATION_SPGU_DRAW_QUAD_HPP_ */
