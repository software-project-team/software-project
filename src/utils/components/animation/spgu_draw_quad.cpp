/*
 * spgu_draw_quad.cpp
 *
 *  Created on: Dec 26, 2021
 *      Author: nasim
 */

#include "spgu_draw_quad.hpp"

namespace spgu {

draw_quad::draw_quad(float width, float height, float offset_x, float offset_y)
:	m_x(0.0f), m_y(0.0f), m_width(width), m_height(height), m_offset_x(offset_x), m_offset_y(offset_y)
{}

}


