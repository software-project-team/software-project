/*
 * spgu_music.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: crystal
 */

#ifndef SRC_UTILS_COMPONENTS_CORE_SPGU_MUSIC_HPP_
#define SRC_UTILS_COMPONENTS_CORE_SPGU_MUSIC_HPP_

#include <string>
#include "raylib.h"

namespace spgu {

/**
 * @brief A class for Music files
 * @author ysf
 *
 * This is a simple class for loading music files to the game.
 */

class music {
public:
	/**
	 * Constructor that loads music to the game from a file
	 *
	 * @param path Path to the music file
	 */
	music(const char* path);

public:
	/**
	 * Music stuct
	 */
	Music m_data;
};
}



#endif /* SRC_UTILS_COMPONENTS_CORE_SPGU_MUSIC_HPP_ */
