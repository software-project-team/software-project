/*
 * spgu_sound.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: crystal
 */

#ifndef SRC_UTILS_COMPONENTS_CORE_SPGU_SOUND_HPP_
#define SRC_UTILS_COMPONENTS_CORE_SPGU_SOUND_HPP_



#include "raylib.h"

namespace spgu {


/**
 * @brief A class for Sound files
 * @author ysf
 *
 * This is a simple class for loading sound files to the game.
 */

class sound {
public:
	/**
	 * Constructor that loads sound to the game from a file
	 *
	 * @param path Path to the sound file
	 */
	sound(const char* path);

public:
	/**
	 * Sound stuct
	 */
	Sound m_data;
};
}


#endif /* SRC_UTILS_COMPONENTS_CORE_SPGU_SOUND_HPP_ */
