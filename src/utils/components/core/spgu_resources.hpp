/*
 * spgu_resources.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_COMPONENTS_CORE_SPGU_RESOURCES_HPP_
#define SRC_UTILS_COMPONENTS_CORE_SPGU_RESOURCES_HPP_

#include "raylib.h"

#include <iostream>
#include <string>
#include <map>
#include <variant>

#include "spgu_music.hpp"
#include "spgu_sound.hpp"
#include "spgu_texture.hpp"



namespace spgu {


enum class res_type {
	music = 0,
	sound = 1,
	texture = 2
};


/**
 * @brief A class for resource management
 * @author ysf
 *
 * This is a simple class for managing resources(music, sound, texture).
 */

class resources {
public:
	/**
	 * Add new resource.
	 *
	 * @param name Name of the resource
	 * @param name Path to the resource file
	 * @param type Type of the resource
	 */
	void add(std::string name, std::string path, spgu::res_type type);

	/**
	 * Get music data.
	 *
	 * @return A Music struct.
	 */
	Music get_music(std::string name);
	/**
	 * Get sound data.
	 *
	 * @return A Sound struct.
	 */
	Sound get_sound(std::string name);
	/**
	 * Get texture data.
	 *
	 * @return A Texture2D struct.
	 */
	Texture2D get_texture(std::string name);

public:
	// TODO try different maps for different resource types
	/**
	 * map of key string and 3 variants of resource type
	 */
	std::map<std::string, std::variant<spgu::music*, spgu::sound*, spgu::texture*>> m_res;
};

}


#endif /* SRC_UTILS_COMPONENTS_CORE_SPGU_RESOURCES_HPP_ */
