/*
 * spgu_story.hpp
 *
 *  Created on: Dec 22, 2021
 *      Author: crystal
 */

#ifndef SRC_UTILS_COMPONENTS_CORE_SPGU_STORY_HPP_
#define SRC_UTILS_COMPONENTS_CORE_SPGU_STORY_HPP_

#include <string>
#include "raylib.h"

namespace spgu {
/**
 * @brief A class which displays the game story
 * @author crystal
 *
 * This is a class which displays the game story.
 */
class story {
public:
	story(std::string story);
	void update();
	void draw(Texture2D bg);
public:
	unsigned int m_frames_counter;
	std::string m_story;

};

}  // namespace spgu



#endif /* SRC_UTILS_COMPONENTS_CORE_SPGU_STORY_HPP_ */
